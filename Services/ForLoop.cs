﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoTask.Services
{
    class ForLoop
    {
        public static void ShowNames()
        {
            string[] namesList = MainArray.NamesArray();
            foreach (string name in namesList)
            {
                if(name == "ken")
                {
                    continue;
                }
                else
                {
                    Console.WriteLine($"Welcome {name}");
                }
            }

            Console.WriteLine("\n");

            for(int i = 0; i < namesList.Length; i++)
            {
                if(namesList[i] == "paul")
                {
                    break;
                } 
                else
                {
                    Console.WriteLine($"{namesList[i]} made it");
                }

            }
        }
    }
}
