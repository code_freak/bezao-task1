﻿using System;
using BezaoTask.Services;

namespace BezaoTask
{
    class Program
    {
        static void Main(string[] args)
        {
            // forloop action
            Console.WriteLine("This is forloop operation:");
            ForLoop.ShowNames();

            Console.WriteLine("\n");

            // while loop action
            Console.WriteLine("This is while loop operation:");
            WhileLoop.ShowNames();

            Console.WriteLine("\n");

            // do while loop action
            Console.WriteLine("This is do while loop operation:");
            DoWhileLoop.ShowMessage();

            Console.WriteLine("\n");

            // switch action
            Console.WriteLine("This is switch operation");
            Switch.LibraryManagementSystem();

            Console.ReadLine();
        }
    }
}
