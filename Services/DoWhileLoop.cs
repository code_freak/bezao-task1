﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoTask.Services
{
    class DoWhileLoop
    {
        public static void ShowMessage()
        {
            string[] namesList = MainArray.NamesArray();
            int i = 0;
            do
            {
                if (namesList[i] == "amy")
                {
                    Console.WriteLine($"{namesList[i]}, you no longer have an account with us");
                }
                i++;
            } while (i < namesList.Length);
        }
    }
}
