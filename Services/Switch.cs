﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoTask.Services
{
    class Switch
    {
        public static void LibraryManagementSystem()
        {
            Console.WriteLine("Choose from the options below which book you're looking for");

            Console.WriteLine("\n");

            Console.WriteLine("c: for computer books");
            Console.WriteLine("m: for mathematics books");
            Console.WriteLine("e: for english books");
            Console.WriteLine("h: for history books");

            Console.WriteLine("\n");

            Console.WriteLine("Enter your option");
            char optionEntered = Convert.ToChar(Console.ReadLine());

            switch(optionEntered)
            {
                case 'c':
                    Console.WriteLine("HTML fundamentals");
                    Console.WriteLine("CSS fundamentals");
                    Console.WriteLine("JS fundamentals");
                    break;
                case 'm':
                    Console.WriteLine("fundamentals of Calculus");
                    Console.WriteLine("Advance Surd");
                    Console.WriteLine("Probability for Seniors");
                    break;
                case 'e':
                    Console.WriteLine("English for newbies");
                    Console.WriteLine("Syntax for seniors");
                    break;
                case 'h':
                    Console.WriteLine("Learning History HIstorically");
                    Console.WriteLine("History made easy");
                    break;
                default:
                    Console.WriteLine("This option not available");
                    break;
            }


        }
    }
}
