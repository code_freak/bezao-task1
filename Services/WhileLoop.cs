﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoTask.Services
{
    class WhileLoop
    {
        public static void ShowNames()
        {
            string[] namesList = MainArray.NamesArray();
            int i = 0;
            while(i < namesList.Length) {
                Console.WriteLine($"Welcome to the loop {namesList[i]}");
                i++;
            }
        }
    }
}
